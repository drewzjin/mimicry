
const _ = require('lodash');
const path = require('path');
const toMimicryFactory = require('./toMimicry');
const resolveFrom = require('resolve-from');

function makeFilepath(file) {
  const directory = path.dirname(file);
  const filename = path.basename(file, '.json');
  return {
    file: `${path.resolve(path.join(directory, filename))}.mimicry.js`,
    mimicFile: `./${filename}.mimicry.json`,
    mimicFilePath: `${path.resolve(path.join(directory, filename))}.mimicry.json`,
  };
}
function requireFromCwd(filePath) {
  const result = path.isAbsolute(filePath)
    ? filePath : path.join(process.cwd(), filePath);
  return require(result);
}

function difference(object, base) {
  function changes(object, base) {
    return _.transform(object, (result, value, key) => {
      if (!_.isEqual(value, base[key])) {
        result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
      }
    });
  }
  return changes(object, base);
}

function runner(activityJsonPath, files, validate = true) {
  const activityJson = requireFromCwd(activityJsonPath);
  const toMimicry = toMimicryFactory(activityJson);

  _.each(files, (file) => {
    const workflow = requireFromCwd(file);
    const output = toMimicry.toMimicry(workflow);
    const outputPaths = makeFilepath(file);
    toMimicry.writeMimicry(output, Object.assign({ eslintAutoFix: false }, outputPaths));
    console.log(`Converted ${file} -> [ ${outputPaths.file}, ${outputPaths.mimicFile} ]`);
    if (validate) {
      try {
        resolveFrom('./', 'mimicry');
        console.log('Local installation found, validating..');
      }
      catch (e) {
        console.log('Local installation of Mimicry is required for validation.');
        return;
      }
      try {
        // Requiring from a generated file gives us a factory, 
        // where we can pass in activityJson and get back a workflow
        // Using the output to validate.
        const workflowFactory = require(outputPaths.file);
        const generatedWorkflow = workflowFactory(activityJson);
        if (_.isEqual(generatedWorkflow, workflow)) {
          console.log(`Validated file ${file} -> [ ${outputPaths.file}, ${outputPaths.mimicFile} ]`);
        } else {
          const diff = require('util').inspect(difference(workflow, generatedWorkflow), false, null);
          console.log(`Found differences ${file} -> [ ${outputPaths.file}, ${outputPaths.mimicFile} ]\nDiff: ${diff}`);
        }
      } catch (e) {
        console.log(`Failed to convert ${file} -> [ ${outputPaths.file}, ${outputPaths.mimicFile}]\nError: ${e}`);
      }
    }
  });
}


module.exports = runner;
