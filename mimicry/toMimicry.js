const _ = require('lodash');
const fs = require('fs');

_.mixin({ pascalCase: _.flow(_.camelCase, _.upperFirst) });

const Workflow = {
  name: 'Workflow',
  properties: {
    args: {
      type: 'block',
      required: true,
    },
  },
};

function metaBuilder(activityJson) {
  activityJson.push(Workflow);
  const activities = {};
  _.each(activityJson, (activity) => {
    const { name, properties } = activity;
    const blocks = _.keys(_.pickBy(activity.properties, r => r.type === 'block'));
    activities[name] = { name, blocks, properties };
  });
  return activities;
}

/**
 * 
 * @param {*} activity object, shaped { @activity { props }}
 * @param {*} activityDocs, shaped { name: activityName, properties, ... }
 * Outputs an object { localVariables, activityProperties, blockProperties }
 * where: localVariables is non-activity, non-block properties attached to the activity
 *        activityProperties are non-block properties documented by the activity class
 *        blockProperties are block properties documented by the activity class
 */
function buildActivityVariables(activity, activityDocs) {
  // Get local variables..
  if (_.isObject(activity) && !_.isArray(activity)) {
    const currentKeys = _.keys(activity);
    const docKeys = _.keys(activityDocs.properties);
    const extraKeys = _.difference(currentKeys, docKeys);
    const localVariables = _.pick(activity, extraKeys);

    // Get the activity properties
    const methodActivityKeys = _.difference(currentKeys, extraKeys, activityDocs.blocks);
    const activityProperties = _.pick(activity, methodActivityKeys);
    if (_.has(activityProperties, '@to')) {
      activityProperties._to = activityProperties['@to'];
      delete activityProperties['@to'];
    }
    // Block properties
    const blockKeys = _.intersection(currentKeys, activityDocs.blocks);
    const blockProperties = _.pick(activity, blockKeys);
    return { localVariables, activityProperties, blockProperties };
  }
  return { localVariables: activity };
}


function getIndent(token) {
  const exitTokens = [']'];
  if (token === '[') return 1;
  if (exitTokens.includes(token)) return -1;
  return 0;
}
function linebreak(token, nextToken) {
  if (_.startsWith(nextToken, '.') && _.endsWith(nextToken, '(')) {
    return true;
  }
  return [',', '['].includes(token);
}
function whitespace(count) {
  let x = '';
  for (let i = 0; i < count; i += 1) {
    // 2
    x += '  ';
  }
  return x;
}

function prettyPrint(tokens) {
  const output = [];
  let current = [];
  let indent = 0;
  _.each(tokens, (token, index) => {
    const blockType = getIndent(token, tokens[index + 1]);
    if (linebreak(token, tokens[index + 1])) {
      current.push(token);
      output.push(whitespace(indent) + current.join(''));
      current = [];
    } else {
      current.push(token);
    }
    indent += blockType;
  });
  output.push(whitespace(indent) + current.join(''));
  const result = output.join('\n');
  return result;
}

class ToMimicry {
  constructor(activityJson) {
    if (_.isObject(activityJson)) {
      this.docs = metaBuilder(activityJson);
    }
    if (_.isString(activityJson)) {
      this.docs = metaBuilder(require(activityJson));
    }
    this.toMimicry = this.toMimicry.bind(this);
    this.getHeaders = this.getHeaders.bind(this);
    this.write = this.writeMimicry.bind(this);
  }
  /**
   * 
   * @param {*} workflow - JS object, same as require(workflow.json)
   * returns { workflow, mimicData }
   * where workflow is a nested, tokenized mimicry.js file
   * and mimicData is the mimicData attached to the workflow JSON.
   */
  toMimicry(workflow) {
    const tokens = [];
    const keys = _.keys(workflow);
    const rawActivityNAme = keys[0];
    const mimicData = {};
    const usedActivities = [];
    if (_.isObject(workflow) && !_.isArray(workflow)) {
      // ** Is an activity, i.e. '@lodash'
      const isActivityWrapper = keys.length === 1 && _.startsWith(rawActivityNAme, '@');
      if (isActivityWrapper) {
        const activityName = _.pascalCase(rawActivityNAme.substring(1, rawActivityNAme.length));
        const {
          localVariables,
          activityProperties,
          blockProperties,
        } = buildActivityVariables(workflow[rawActivityNAme], this.docs[activityName]);
        usedActivities.push(activityName);
        tokens.push(activityName);
        tokens.push('(');

        // We have local variables here! 'mimic' is a local variable.
        // Modify the interaction with Mimic.
        // First, drop mimic
        if (_.isObject(localVariables) && _.has(localVariables, 'mimic')) {
          // Pluck mimic
          const { mimic } = localVariables;
          const id = mimic.id || 'rootMimic';
          const shrunkMimicData = { id };
          // Put into our map
          mimicData[id] = mimic;
          // Replace with smaller
          localVariables.mimic = shrunkMimicData;
        }
        tokens.push(JSON.stringify(localVariables));
        tokens.push(')');
        // Start chaining activity properties
        _.each(activityProperties, (propVal, propKey) => {
          tokens.push(`.${propKey}(`);
          tokens.push(JSON.stringify(propVal));
          tokens.push(')');
        });
        // Start chaining block properties
        if (_.isObject(blockProperties) && !_.isArray(blockProperties)) {
          _.each(blockProperties, (propVal, propKey) => {
            tokens.push(`.${propKey}(`);
            // recursion part
            const { tokens: parsedTokens, mimicData: parsedMimicData, usedActivities: previousUsedActivities } = this.toMimicry(propVal);
            Object.assign(mimicData, parsedMimicData);
            usedActivities.push(previousUsedActivities);
            tokens.push(parsedTokens);
            // end ^
            tokens.push(')');
          });
        }
      }
    }
    if (_.isArray(workflow)) {
      tokens.push('[');
      workflow.forEach((r) => {
        const { tokens: parsedTokens, mimicData: parsedMimicData, usedActivities: previousUsedActivities } = this.toMimicry(r);
        Object.assign(mimicData, parsedMimicData);
        usedActivities.push(previousUsedActivities);
        tokens.push(parsedTokens);
        tokens.push(',');
      });
      tokens.push(']');
    }
    if (_.isString(workflow)) {
      tokens.push(`"${workflow}"`);
    }
    return { tokens, mimicData, usedActivities };
  }

  writeMimicry(mimicry, opts = { eslintAutoFix: false }) {
    const { mimicData, tokens, usedActivities } = mimicry;
    const uniqueUsedActivities = _.uniq(_.flattenDeep(usedActivities));
    const workflow = _.flattenDeep(tokens);
    const fileContentArray = [];
    const filename = `output-${Date.now() / 1000}`;
    opts.file = opts.file || `${filename}.mimicry.js`;
    opts.mimicFile = opts.mimicFile || `${filename}.mimicry.json`;
    fileContentArray.push(this.getHeaders(uniqueUsedActivities));
    fileContentArray.push(`const workflow = ${prettyPrint(workflow)};`);
    fileContentArray.push(`const mimicData = require('${opts.mimicFile}')`);
    fileContentArray.push('return parse(workflow, mimicData);');
    // This is the closing tag for the factory function
    fileContentArray.push('}');
    fileContentArray.push('module.exports = factory');
    let fileContent = fileContentArray.join('\n');
    if (opts.eslintAutoFix) {
      try {
        const { CLIEngine, Linter } = require('eslint');
        const linter = new Linter();
        const cli = new CLIEngine();
        // needs a random file to grab config for.. this makes CLIEngine build config from .eslintrc.js
        const linterConfig = cli.getConfigForFile('./index.js');
        // the more it runs, the better it gets!
        let isDone = false;
        while (!isDone) {
          // fixed doesn't mean it's done, fixed means it fixed
          // keep fixing until it did not fix
          const result = linter.verifyAndFix(fileContent, linterConfig);
          const { fixed, output } = result;
          isDone = !fixed;
          fileContent = output;
        }
      } catch (e) {
        console.log('Error trying to Eslint the Mimicry file', e);
        return null;
      }
    }
    fs.writeFileSync(opts.file, fileContent);
    fs.writeFileSync(opts.mimicFilePath, `${JSON.stringify(mimicData)}`);
    return opts.file;
  }

  getHeaders(activities) {
    const header = [];
    // header.push(`console.log('paths', module.paths);`)
    header.push(`const { mimicry } = require('mimicry');`);
    header.push('\n\nfunction factory(activityJson) {');
    header.push('\nconst { parse, ');
    header.push(activities.join(', '));
    header.push('} = mimicry(activityJson);');
    return header.join('');
  }
}
function factory(activityJson) {
  return new ToMimicry(activityJson);
}
module.exports = factory;
