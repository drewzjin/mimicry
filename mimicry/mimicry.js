const _ = require('lodash');

class Base {
  constructor(config) {
    this.setProperty.bind(this);
    this.getProperty.bind(this);
    this.get.bind(this);
    this.activityProperties = config.activityProperties || {};
    this.activityVariables = {};

    this.blocks = config.blocks;
    this.name = config.name;
    this.properties = config.properties || {};

    _.each(this.properties, (propertyValue, propertyKey) => {
      // Add an alias because calling it is hard
      if (propertyKey === '@to') {
        this._to = value => this.setProperty(propertyKey, value);
      }
      this[propertyKey] = value => this.setProperty(propertyKey, value);
    });
  }

  get(mimicData) {
    let props;
    if (_.isObject(this.activityProperties)) {
      const method = _.isArray(this.activityProperties) ? 'map' : 'mapValues';
      props = _[method].apply(this, [this.activityProperties, (v, k) => {
        if (!this.blocks.includes(k)) {
          return v;
        }
        if (!_.isArray(v)) {
          return v.get(mimicData);
        }
        return _.map(v, (v2) => {
          // Could be a Base class, or just an expression: "=this.greeting='Hello'""
          if (!_.isPlainObject(v2) && !_.isString(v2)) {
            return v2.get(mimicData);
          }
          return v2;
        });
      }]);
    } else {
      props = this.activityProperties;
    }

    // If it has a mimic property, overwrite it from the one we have from mimicData... might be naive to think this way
    if (_.has(props, 'mimic')) {
      props.mimic = mimicData[props.mimic.id];
    }
    return {
      [`@${_.camelCase(this.name)}`]: props,
    };
  }

  setProperty(k, v) {
    this.activityProperties[k] = v;
    return this;
  }
  getProperty(k) {
    return this.activityProperties[k];
  }
}

const Workflow = {
  name: 'Workflow',
  properties: {
    args: {
      type: 'block',
      required: true,
    },
  },
};

function activityConstructor(config) {
  return activityProperties => new Base(Object.assign({}, config, { activityProperties }));
}

function buildFunctions(activityJson) {
  // Workflow is not documented, so push in a raw doc
  activityJson.push(Workflow);
  const activityConstructors = {};
  _.each(activityJson, (activity) => {
    const { name, properties } = activity;
    const blocks = _.keys(_.pickBy(activity.properties, r => r.type === 'block'));
    activityConstructors[name] = activityConstructor({ name, blocks, properties });
  });
  return activityConstructors;
}

function parse(tree, mimicData) {
  if (!_.isPlainObject(tree)) {
    return tree.get(mimicData);
  }
  return null;
}

function mimicry(activityJson) {
  return Object.assign({
    parse,
  },
    buildFunctions(activityJson));
}

module.exports = mimicry;
